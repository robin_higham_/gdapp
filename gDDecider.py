
import random
import json

from gDEntry import GDEntry

client = 0
db = 0

whitelist = ["Genre","Theme","Prop"]

class GDDecider():

    def __init__(self,db):
        self.data = []
        self.db = db

    def add(self, args):
        requiredArgs = [args.value,args.add]

        if(all(v is not None for v in requiredArgs)):
            if(args.add in whitelist):
                g = GDEntry(self.db)
                g.add(args.value,args.add)
            else:
                print('Collection Not Found!')

    
    def generate(self):

        genre = ''
        theme = ''
        props = []

        proplevel = random.randrange(10) 

        for x in range(0, proplevel):
            props.append(self.db['Prop'].find()[random.randrange(self.db['Prop'].estimated_document_count())])

        genre = self.db['Genre'].find()[random.randrange(self.db['Genre'].estimated_document_count())]
        theme = self.db['Theme'].find()[random.randrange(self.db['Theme'].estimated_document_count())]

        data = {
            'genre':genre,
            'theme':theme,
            'props':props
        }

        self.db["Generated"].insert_one(data)

    def get(self):
        return self.db['Generated'].find_one(limit=1)

            

