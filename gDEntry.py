
class GDEntry():
    def __init__(self,db):
        self.db = db
    
    def add(self,value,collection):
        data = {
            'value':value
        }
        if(self.db[collection].count_documents(data) < 1):
            print('added ' + value + ' to: ' + collection)
            self.db[collection].insert_one(data)
        else:
            print('>>DUPLICATION<<')


