class pymongoConfig:
    def __init__(self, ipAddress, port):
        self.ipAddress = ipAddress
        self.port = port

    def getIP(self):
        print(self.ipAddress)
        return self.ipAddress

    def getPort(self):
        print(self.port)
        return self.port