
import SimpleHTTPServer 
import SocketServer
import json
from gDDecider import GDDecider
from gDcsv2Mongo import GDcsv2Mongo
from pymongo import MongoClient
import argparse
import config

PORT = 1337

c = config.pymongoConfig("127.0.0.1",27017)
client = MongoClient(c.ipAddress, c.port)

d = GDDecider(client['GDAPP'])
csv2Mongo = GDcsv2Mongo(client['GDAPP'])

class server(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):

        
        props = []
        parsed = d.get()

        for x in parsed["props"]:
            props.append(x['value'])

        data = {
            "genre":parsed["genre"]['value'],
            "theme":parsed["theme"]['value'],
            "prop":props           
        }

        self.send_response(200)
        self.send_header('Content-type','text/json')
        self.end_headers()
        self.wfile.write(json.dumps(data))

def runServer():

    try:
        print("Running as http server.... on Port:"+ str(PORT))
        httpd = SocketServer.TCPServer(("", PORT), server)
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("http server closing down...")
        httpd.server_close()

def main():
    parser = argparse.ArgumentParser(description='Options for the Decider')

    parser.add_argument('--add', help='Set collection to add to.')
    parser.add_argument('--value', help='Add a value to a field.')
    parser.add_argument('-csv', help='read and parse CSV to database')
    parser.add_argument('-generate', help='Request server generate new entry.')
    parser.add_argument('--http', help='Launch as server.')

    args = parser.parse_args()

    if(args.http != None):
        runServer()
    else:
        if(args.generate != None):
            d.generate()
        else:
            if(args.csv != None):
                csv2Mongo.parse(args.csv)
            else:
                d.add(args)

main()

